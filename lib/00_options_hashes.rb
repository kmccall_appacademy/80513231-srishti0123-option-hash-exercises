# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```

# require 'byebug'
# def transmogrify(string, options = {})
#
#   debugger
#   defaults = {times: 2, upcase: false, reverse: true}
#
#   defaults.merge!(options)
#
#   string = string * defaults[:times] if options[:times]
#   string.upcase! if defaults[:upcase]
#   string.reverse! if defaults[:reverse]
# end

def transmogrify(string, options = {})

  defaults = {times:2, upcase:true, reverse:false}

  defaults.merge!(options)

  string = string * defaults[:times]
  string.upcase! if defaults[:upcase]
  string.reverse! if defaults[:reverse]
end
